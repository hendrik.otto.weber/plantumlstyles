# PlantUML Styles

Styles for uniform plantuml diagrams

## Generate images

After changing / adding styles or examples run `generate.bat`

Requires environment variable PLANTUML_BIN to reference the java execution of the PlantUML Jar e.g. `java -jar D:\Software\PlantUML\plantuml.jar`

## Available styles

### Monokai

![Monokai](output/monokai/class.svg)

`!includeurl https://git.rwth-aachen.de/hendrik.otto.weber/plantumlstyles/-/raw/master/styles/monokai.cfg`


### Inst

![Inst](output/inst/activity.svg)

`!includeurl https://git.rwth-aachen.de/hendrik.otto.weber/plantumlstyles/-/raw/master/styles/inst.cfg`

### Fors

![Fors](output/fors/component.svg)

`!includeurl https://git.rwth-aachen.de/hendrik.otto.weber/plantumlstyles/-/raw/master/styles/fors.cfg`
