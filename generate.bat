for %%s in (styles\*.cfg) do (
	for %%f in (examples\*.plantuml) do (
		echo %%f
		%PLANTUML_BIN% -charset utf-8 -config "%%s" -o "..\output\%%~ns" -tsvg "%%f"
	)
)